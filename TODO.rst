.. default-role:: code

########################
 Things left to be done
########################

API
   - The definitions for `custom-print`, `custom-write` and `custom-display`
     are only placeholders

Client
   - In the type definitions the message ID has to be a 32-bit number, not an
     arbitrary integer. This restriction should be made part of the definition
